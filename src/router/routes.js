
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') }
    ]
  },
  {
    path: '/form',
    component: () => import('components/CreateForm')
  },
  {
    path: '/',
    component: () => import('components/newsMain'),
    children: [
      { path: '', component: () => import('components/newsMain') }
    ]
  },
  {
    path: '/info',
    component: () => import('components/infoNews'),
    children: [
      { path: '', component: () => import('components/newsMain') }
    ]},
  {path: '/news-details/:newsId',
    params: true,
    name: 'details',
    component: () => import('components/newsDetails.vue')
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  },
]

export default routes
