// 1.https://floating-mesa-98493.herokuapp.com/news/add
//   method:'post',
//     data:{
//   title: {
//     type: String,
//       required: true
//   },
//   date: {
//     type: Date,
//       required: true
//   },
//   image: {
//     type: String
//   },
//   news: {
//     type: String,
//       required: true
//   }
// }
// 2.https://floating-mesa-98493.herokuapp.com/news/getAll
//   3.https://floating-mesa-98493.herokuapp.com/news/getOne/:id

import axios from "axios";
const url = 'https://floating-mesa-98493.herokuapp.com/news'

export function getAllNews (context, commit) {
  axios({
    method: 'GET',
    url: `${url}/getAll`
  })
    .then((result) => {
      context.commit('newsList', result.data.news)
  })
    .catch((error) => {
      console.log('Error from getAllNews action', error)
  })
}

export function getNewsById (context, id) {
  axios({
    method: 'GET',
    url: `${url}/getOne/${id}`
  })
    .then((result) => {
      context.commit('newsID', result.data.news)
    })
    .catch((error) => {
      console.log('Error from getNewsById action', error)
    })
}
