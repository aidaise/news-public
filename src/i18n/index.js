import ruRU from './ru-RU'
import enUS from './en-US'
import kgKG from './kg-KG'

export default {
  'ru-RU': ruRU,
  'en-US': enUS,
  'kg-KG': kgKG
}
