// This is just an example,
// so you can safely delete all default props below

export default {
  Aboutcompany:'About company',
  Gazpromtoday:'Gazprom today',
  History:'Gas history',
  Guide:'industries',
  Standard:'Management',
  Vacancies:'Legal and regulatory framework',
  Responsibility:'Vacancies',
  Charity:'Social responsibility',
  Sponsorship:'Sponsorship',
  Security:'Industrial safety',
  Protection:'Environmental protection',
  Presscenter:'Press center ',
  News:'news',
  Massmedia:'Mass Media about us',
  Speaker:'Head column',
  Gallery:'Photo gallery',
  Video:'Video gallery',
  Fuel:'NVG fuel',
  Description:'Description',
  Advantages:'Benefits of natural gas',
  Scheme:'CNG filling station layout',
  Manufacturers:'Worldwide CNG producers',
  HBO:'HBO',
  Implementation:'Gas sales',
  Individuals:'For individuals',
  Legalentities:'Legal entities',
  Indications:'Transmitting readings',
  Payment:'Payment for gas',
  Tariffs:'Tariffs',
  Installation:'Installation / verification of gas meters',
  Service:'Service centers',
  Procurement:'Procurement',
  Results:'Procurement results',
  Position: 'Procurement Regulation',
  Personalarea:'Personal Area',
  Testimony:'Transfer readings',
  Pay:'Pay for gas',
  Background:' Background',
  Emergency:'Emergency',
  Message:'To write a message',
  Contacts:'Contacts',
  Address:'Bishkek, st. Tokombayeva, 23/2 (Legal address: st.Gorky, 22)',
  Connection:'Tel: +996 (312) 53 00 35 Fax: +996 (312) 53 00 33 Emal: pr@gazprom.kg',
  Fil:'Departments Branches',
  LLC:'© 2019 Gazprom Kyrgyzstan LLC. All rights reserved. State registration certificate No. 0024491.',
  failed: 'Action failed',
  title: 'The event was opened by the head of the Oktyabrskaya regional administration Bolot Alymkulov,\n' +
    '\n' +
    'Mayor of Bishkek Kubanychbek Kulmatov congratulated everyone on the holiday and wished health, prosperity and a' +
    ' peaceful sky overhead.\n' +
    '\n' +
    '"Today we celebrate the 72nd anniversary of the complete liberation of Leningrad from the Nazi blockade.' +
    ' I cordially congratulate everyone on the holiday! Eternal glory to all those who fought for the Motherland,' +
    ' famous and nameless fighters. We bow before the living and those who are not with us. peace and prosperity will' +
    ' always be in your homes, "Kubanychbek Kulmatov said.' +
    'Anna Kutanova, the chairman of the Kyrgyz Society of the Siege of Leningrad, came to remember and pay' +
    ' tribute to the memory of the victims, who congratulated everyone on this great date and read poems dedicated' +
    ' to the memory of the victims of the blockade.\n' +
    '\n' +
    'After that, 900 candles were lit - equal to the number of days of the blockade. At the end of lighting candles,' +
    ' all comers laid flowers at the monument.' +
    'The meeting was attended by Adviser to the President of the Kyrgyz Republic Busurmankul Tabaldiev,' +
    ' Plenipotentiary and Extraordinary Ambassador of the Russian Federation to the Kyrgyz Republic Andrei Krutko,' +
    ' representatives of the Gazprom company, as well as the blockade of Leningrad and veterans of the Great Patriotic' +
    ' War.' +
    'Reference: Today 18 blockade of Leningrad live in Bishkek. In order to provide targeted social assistance, ' +
    'the city budget allocates a one-time financial aid, a quarterly stipend of 1200 soms, free travel on ' +
    'city public transport; a one-time allowance by May 9 in the amount of 5 thousand soms, funds for travel ' +
    'to visit the hero-city of Leningrad. According to the Decree of the President of the Kyrgyz Republic,' +
    ' participants, invalids of the Great Patriotic War, the siege of Leningrad, prisoners of concentration camps ' +
    'are paid monetary compensation at the expense of the Republican budget in return for benefits in the amount of' +
    ' 7 thousand soms.',
  success: 'Action was successful',
  about: 'About company',
  press: 'Press center',
  social: 'Social responsibility',
  gas: 'Gas realization',
  fuel: 'NGV fuel',
  buy: 'Procurement',
  contacts: 'Contacts',
  emergency: 'Emergency',
  call: 'Referential',
  langs: 'Languages',
  search: 'Search',
  area: 'Personal area'
}
